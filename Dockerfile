FROM alpine:3.8

ARG WORK_DIR=/var/www/node
RUN mkdir -p ${WORK_DIR}
WORKDIR ${WORK_DIR}

RUN apk update && apk upgrade
RUN apk add --no-cache bash cmake make gcc g++ boost-system boost-thread boost-dev

COPY . .
RUN cmake . && make
RUN chmod +x divisionWeb
ENTRYPOINT [ "./divisionWeb" ]