#include "crow_all.h"
#include "div.h"

int main()
{
    crow::SimpleApp app;

    CROW_ROUTE(app, "/")
    ([]() {
        return "Use /division/<int>/<int>";
    });

    CROW_ROUTE(app, "/json")
    ([] {
        crow::json::wvalue x;
        x["message"] = "Use /division/<int>/<int>";
        return x;
    });

    CROW_ROUTE(app, "/division/<int>/<int>")
    ([](int dividend, int divisor) {
        if (divisor == 0)
            return crow::response(400);
        int quotient = division(dividend, divisor);
        std::ostringstream os;
        os << "Result: " << quotient;
        return crow::response(os.str());
    });

    app.port(18080).multithreaded().run();
}